package sharp.teknos.dev.sharp1;

public class MyAnotherClass {

    private MyClass myClass = new MyClass() {
        @Override
        public void callMe() {

        }
    };

    public MyAnotherClass(){
        myClass.showMe();
    }
}
