package sharp.teknos.dev.sharp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";
    private TextView txView;

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.w(TAG,"Plus btn called");
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txView = (TextView) findViewById(R.id.txtView);
        Button plusBtn = (Button) findViewById(R.id.plusBtn);

        plusBtn.setOnClickListener(onClickListener);

        Log.w(TAG,"Create called");
    }

    public void onBtnPressed(View view){
        Button button = (Button)view;
        txView.append(button.getText().toString());
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.w(TAG,"Resume called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.w(TAG,"Pause called");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.w(TAG,"Stop called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.w(TAG,"Destroy called");
    }
}
